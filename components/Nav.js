import Link from "next/link"

export default function Header() {
  return (
    <div className="logout-nav">
      <p>How to Use</p>
      <p>Sign In</p>
      <Link href="./AddTask">
        <img src="/add.svg" />
      </Link>
    </div>
  )
}

export default function NoTasksFound() {
  return (
    <div className="task notask">
      <h4>No Tasks To Show!</h4>
      <p>click + in top bar to add one</p>
    </div>
  )
}

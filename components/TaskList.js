export default function TaskList({ task, Tasks, setTasks }) {
  const handleRemove = (e) => {
    setTasks(Tasks.filter((t) => t.id != task.id))
  }

  return (
    <div className="task">
      <h5 onClick={handleRemove} className="removeTask">
        remove
      </h5>
      <h5>→ {task.title}</h5>
      <p className="description">{task.description}</p>
      <div className="dates">
        {task.forDate ? <p className="forDate">For {task.forDate}</p> : null}
        {task.dueDate ? <p className="dueDate">Due on {task.dueDate}</p> : null}
      </div>
    </div>
  )
}

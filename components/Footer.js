export default function Header() {
  return (
    <footer>
      <a href="https://peste.dev" target="_blank" rel="noopener noreferrer">
        Powered by <strong> AmirT</strong>
      </a>
    </footer>
  )
}

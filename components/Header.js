import Link from "next/link"

import Nav from "../components/Nav"

export default function Header() {
  return (
    <header>
      <div className="header-content">
        <Link href="./">
          <a>
            <h3>Agenda App</h3>
          </a>
        </Link>
        <Nav />
      </div>
    </header>
  )
}

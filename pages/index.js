import { useState, useEffect } from "react"
import Head from "next/head"
import Link from "next/link"

import { Calendar, momentLocalizer } from "react-big-calendar"
import "react-big-calendar/lib/css/react-big-calendar.css"
import moment from "moment"
const localizer = momentLocalizer(moment)

import TaskList from "../components/TaskList"
import NoTasksFound from "../components/NoTasksFound"
import Header from "../components/Header"
import Footer from "../components/Footer"

export default function Home() {
  const [Tasks, setTasks] = useState([""])
  useEffect(() => {
    if (localStorage.getItem("Agenda-data")) {
      setTasks(JSON.parse(localStorage.getItem("Agenda-data")))
    }
  }, [])
  useEffect(() => {
    localStorage.setItem("Agenda-data", JSON.stringify(Tasks))
  }, [Tasks])

  const asapTasks = Tasks.filter((task) => task.cat == "Asap")
  const laterTasks = Tasks.filter((task) => task.cat == "Later")

  const hexIsLight = function (color) {
    const hex = color.replace("#", "")
    const c_r = parseInt(hex.substr(0, 2), 16)
    const c_g = parseInt(hex.substr(2, 2), 16)
    const c_b = parseInt(hex.substr(4, 2), 16)
    const brightness = (c_r * 299 + c_g * 587 + c_b * 114) / 1000
    return brightness > 155
  }

  const eventStyler = function (task) {
    let eventStyle = {
      backgroundColor: "#618a84",
      color: "#eee",
      fontSize: "13px",
      textAlign: "right",
    }
    if (task.color) {
      eventStyle.backgroundColor = task.color
      hexIsLight(task.color) ? (eventStyle.color = "#222") : (eventStyle.color = "#eee")
    }
    return {
      className: "",
      style: eventStyle,
    }
  }

  return (
    <div className="container">
      <Head>
        <title>On Your Agenda Today!</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header />

      <main>
        <div className="grid">
          <div className="calendar">
            <h3>Monthly View</h3>
            <Calendar localizer={localizer} events={Tasks} style={{ height: "70vh" }} startAccessor="start" endAccessor="end" rtl={true} eventPropGetter={(task) => eventStyler(task)} />
          </div>

          <div className="section">
            <div className="card">
              <h3>Do ASAP</h3>
              {asapTasks.length ? asapTasks.map((task) => <TaskList task={task} Tasks={Tasks} setTasks={setTasks} />) : <NoTasksFound />}
            </div>
            <div className="card">
              <h3>Do Later</h3>
              {laterTasks.length ? laterTasks.map((task) => <TaskList task={task} Tasks={Tasks} setTasks={setTasks} />) : <NoTasksFound />}
            </div>
          </div>
        </div>
      </main>

      <Footer />
    </div>
  )
}

import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"

import Header from "../components/Header"
import Footer from "../components/Footer"
import { useEffect, useState } from "react"

import momentJalaali from "moment-jalaali"
import DatePicker from "react-datepicker2"

import { GithubPicker } from "react-color"

export default function AddTask() {
  const router = useRouter()

  const defaultCats = ["Asap", "Later"]

  const [Tasks, setTasks] = useState([""])
  const [allCats, setallCats] = useState(defaultCats)

  useEffect(() => {
    if (localStorage.getItem("Agenda-data")) {
      setTasks(JSON.parse(localStorage.getItem("Agenda-data")))
    }
  }, [])
  useEffect(() => {
    localStorage.setItem("Agenda-data", JSON.stringify(Tasks))
  }, [Tasks])

  const [cat, setCat] = useState()
  const [title, setTitle] = useState()
  const [description, setDescription] = useState()
  const [start, setStart] = useState()
  const [end, setEnd] = useState()
  const [forDate, setForDate] = useState()
  const [dueDate, setDueDate] = useState()
  const [color, setColor] = useState()

  const handleSubmit = (e) => {
    e.preventDefault()
    setTasks([...Tasks, { id: new Date(), cat, title, description, forDate, dueDate, start, end, color }])
    router.push("/")
  }

  return (
    <div className="container">
      <Head>
        <title>Add to Agenda</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header />

      <main>
        {/* Add a Single Task */}
        <div className="card">
          <h3>Add a new Task</h3>

          <form onSubmit={handleSubmit} className="task">
            <label>Category</label>
            <div className="radios">
              {allCats.map((cat) => {
                return (
                  <section onChange={(e) => setCat(e.target.value)}>
                    <input type="radio" id={cat} name="cat" value={cat} required></input>
                    <label htmlFor={cat}>{cat}</label>
                  </section>
                )
              })}
            </div>
            <label htmlFor="title">Title</label>
            <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} id="title" name="title" required />
            <label htmlFor="forDate">Date</label>
            <tag>
              <DatePicker
                isGregorian={false}
                timePicker={false}
                onChange={(date) => {
                  setStart(date)
                  setForDate(date.format("jYYYY/jM/jD"))
                }}
              />
            </tag>
            <label htmlFor="dueDate">Deadline</label>
            <tag>
              <DatePicker
                isGregorian={false}
                timePicker={false}
                onChange={(date) => {
                  setEnd(date)
                  setDueDate(date.format("jYYYY/jM/jD"))
                }}
              />
            </tag>
            <label htmlFor="description">Description</label>
            <textarea value={description} onChange={(e) => setDescription(e.target.value)} id="description" name="description" rows="3" cols="50" maxlength="180"></textarea>
            <label htmlFor="color">Color</label>
            <GithubPicker onChange={(e) => setColor(e.hex)} id="color" name="color" />
            <div></div>
            <input type="submit" id="cat" />
          </form>
        </div>

        {/* Add a Task Category */}
      </main>

      <Footer />
    </div>
  )
}
